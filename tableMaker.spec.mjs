import tableMaker from "./tableMaker.mjs";
import assert from "assert";

assert.strictEqual(
  tableMaker([]),
  ``,
  `Empty array should return an empty string.`
);

assert.strictEqual(
  tableMaker([1]),
  `╔═════╗
║  1  ║
╚═════╝
`,
  `1 dimension array should return an array.`
);

assert.strictEqual(
  tableMaker([1, "abc"]),
  `╔═══════╗
║  1    ║
║  abc  ║
╚═══════╝
`,
  `1 dimension array should return a vertical array.`
);

assert.strictEqual(
  tableMaker([1, "abc"], { headerBorder: true }),
  `╔═══════╗
║  1    ║
╠═══════╣
║  abc  ║
╚═══════╝
`,
  `1 dimension vertical array should be respect headerBorder.`
);

assert.strictEqual(
  tableMaker([1, "abc"], { allMiddleBorders: true }),
  `╔═══════╗
║  1    ║
╠═══════╣
║  abc  ║
╚═══════╝
`,
  `1 dimension vertical array should be respect allMiddleBorders.`
);

assert.strictEqual(
  tableMaker([[1], ["abc"]]),
  `╔═══════╗
║  1    ║
║  abc  ║
╚═══════╝
`,
  `2 dimension array should return a valid array.`
);

assert.strictEqual(
  tableMaker([[1], ["abc"], ["efg"], ["H"]], { headerBorder: true }),
  `╔═══════╗
║  1    ║
╠═══════╣
║  abc  ║
║  efg  ║
║  H    ║
╚═══════╝
`,
  `2 dimensions vertical array should be respect headerBorder.`
);


assert.strictEqual(
  tableMaker([[1], ["abc"], ["efg"], ["H"]], { allMiddleBorders: true }),
  `╔═══════╗
║  1    ║
╠═══════╣
║  abc  ║
╠═══════╣
║  efg  ║
╠═══════╣
║  H    ║
╚═══════╝
`,
  `2 dimensions vertical array should be respect allMiddleBorders.`
);

assert.strictEqual(
  tableMaker([
    ["hello", "this", "is"],
    ["a", "custom", "table"],
    ["with", "3 rows", "3 cols"],
  ]
  ),
  `╔═════════╦══════════╦══════════╗
║  hello  ║  this    ║  is      ║
║  a      ║  custom  ║  table   ║
║  with   ║  3 rows  ║  3 cols  ║
╚═════════╩══════════╩══════════╝
`,
  `3 rows × 3 cols without inner borders.`
);

assert.strictEqual(
  tableMaker([
    ["hello", "this", "is"],
    ["a", "custom", "table"],
    ["with", "3 rows", "3 cols"],
  ],
    { headerBorder: true }
  ),
  `╔═════════╦══════════╦══════════╗
║  hello  ║  this    ║  is      ║
╠═════════╬══════════╬══════════╣
║  a      ║  custom  ║  table   ║
║  with   ║  3 rows  ║  3 cols  ║
╚═════════╩══════════╩══════════╝
`,
  `3 rows × 3 cols with header borders.`
);

assert.strictEqual(
  tableMaker([
    ["hello", "this", "is"],
    ["a", "custom", "table"],
    ["with", "3 rows", "3 cols"],
  ],
    { allMiddleBorders: true }
  ),
  `╔═════════╦══════════╦══════════╗
║  hello  ║  this    ║  is      ║
╠═════════╬══════════╬══════════╣
║  a      ║  custom  ║  table   ║
╠═════════╬══════════╬══════════╣
║  with   ║  3 rows  ║  3 cols  ║
╚═════════╩══════════╩══════════╝
`,
  `3 rows × 3 cols with inner borders.`
);

assert.strictEqual(
  tableMaker([[1]], { padding: 1 }),
  `╔═══╗
║ 1 ║
╚═══╝
`,
  `horizontal padding as param 1.`
);

/************************************
 * { theme: "light" }
 ************************************/

assert.strictEqual(
  tableMaker([1], { theme: `light` }),
  `┌─────┐
│  1  │
└─────┘
`,
  `1 dimension array should return an array.`
);

assert.strictEqual(
  tableMaker([
    ["hello", "this", "is"],
    ["a", "custom", "table"],
    ["with", "3 rows", "3 cols"],
  ],
    { headerBorder: true, theme: `light` }
  ),
  `┌─────────┬──────────┬──────────┐
│  hello  │  this    │  is      │
├─────────┼──────────┼──────────┤
│  a      │  custom  │  table   │
│  with   │  3 rows  │  3 cols  │
└─────────┴──────────┴──────────┘
`,
  `3 rows × 3 cols with header borders.`
);

// tl: `┌`,
// tr: `┐`,
// tc: `┬`,
// h: `─`,
// v: `│`,
// bl: `└`,
// br: `┘`,
// bc: `┴`,
// ml: `├`,
// mc: `┼`,
// mr: `┤`,