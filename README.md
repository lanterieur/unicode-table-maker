# Unicode Table Maker

A very simple package to draw unicode tables from 2D arrays of strings.

#### Why not use console.table?

I made this package to use in simple CLI interfaces.

```
import tableMaker from "./tableMaker.mjs";

console.log(
  tableMaker(
  [
    ["hello","this","is"],
    ["a","custom","table"],
    ["with","3 rows","3 cols"],
  ],
  {headerBorder:true}
  )
);
```

Gives:

```
╔═════════╦══════════╦══════════╗
║  hello  ║  this    ║  is      ║
╠═════════╬══════════╬══════════╣
║  a      ║  custom  ║  table   ║
║  with   ║  3 rows  ║  3 cols  ║
╚═════════╩══════════╩══════════╝
```

## Arguments

tableMaker takes two arguments:

- `arr`: the table to draw.
- `options`: optional generation options
  - `options.headerBorder`: bool flag to trigger a separation line between the header row and the table body.
  - `options.allMiddleBorders`: bool flag to enable all horizontal lines between rows. Includes header row.
  - `options.padding`: int size of the horizontal padding in a cell.
  - `options.theme`: "double" or "light". Sets the style of the table borders.

```
tableMaker(arr, {
  padding: 4,
  headerBorder: true,
  theme: "light"
});
┌─────────────┬──────────────┬──────────────┐
│    hello    │    this      │    is        │
├─────────────┼──────────────┼──────────────┤
│   a         │    custom    │    table     │
│   with      │    3 rows    │    3 cols    │
└─────────────┴──────────────┴──────────────┘
```
