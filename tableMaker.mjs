//@ts-check
/**
 * @typedef PieceName
 * @type {"tl"|"tr"|"tc"|"h"|"v"|"bl"|"br"|"bc"|"ml"|"mc"|"mr"}
 */
const borderCharsDouble = {
  tl: `╔`,
  tr: `╗`,
  tc: `╦`,
  h: `═`,
  v: `║`,
  bl: `╚`,
  br: `╝`,
  bc: `╩`,
  ml: `╠`,
  mc: `╬`,
  mr: `╣`,
};
const borderCharsLight = {
  tl: `┌`,
  tr: `┐`,
  tc: `┬`,
  h: `─`,
  v: `│`,
  bl: `└`,
  br: `┘`,
  bc: `┴`,
  ml: `├`,
  mc: `┼`,
  mr: `┤`,
};
/**
 * @typedef CharGetter
 * @type {(v:PieceName)=>string}
 */
/**
 * Curryied function to get a border character
 * @param {"light"|"double"} theme Table theme
 * @returns {CharGetter}
 */
function borderChars(theme = `double`) {
  /**
   * Returns a piece of the themed border character set
   * @param {PieceName} piece
   * @returns {String}
   */
  return function (piece) {
    return theme === `double`
      ? borderCharsDouble[piece]
      : borderCharsLight[piece]
  };
}
const defaultPadding = 2;
/**
 * @typedef {Object} TableOptions
 * @prop {Boolean} [headerBorder]
 * @prop {Boolean} [allMiddleBorders]
 * @prop {Number} [padding]
 * @prop {"light"|"double"} [theme]
 */
/**
 * @param {string[][]} arr
 * @param {TableOptions} [options]
 */
export default function tableMaker(arr, options) {
  if (!Array.isArray(arr) || !arr[0]) return ``;
  // ensure array of arrays of strings
  arr = arr.map((v) =>
    Array.isArray(v) ? v.map((vv) => (!vv ? "" : String(vv))) : [String(v)]
  );

  // assuming all columns are populated
  const rowCount = arr.length;
  const colCount = arr[0].length;

  // compute col widths
  /** @type {Array<Number>} */
  const colWidths = Array(colCount).fill(0);
  for (let r = 0; r < rowCount; r++) {
    for (let c = 0; c < colCount; c++) {
      colWidths[c] = Math.max(colWidths[c], arr[r][c].length);
    }
  }
  return arr.reduce(ArrRowToTableRow(rowCount, colWidths, options), ``);
}

/**
 * Draws table rows
 * @param {Number} height Number of rows in the table
 * @param {Array<Number>} colWidths Width of the columns
 * @param {TableOptions} [options]
 */
function ArrRowToTableRow(height, colWidths, options) {
  const padding = options?.padding || defaultPadding;
  /**
   * @param {String} tableString
   * @param {Array<String>} row
   * @param {Number} rowIndex
   * @returns {String}
   */
  return function arrRowToTableRow(tableString, row, rowIndex) {
    const borderCharSet = borderChars(options?.theme);
    const line =
      borderCharSet(`v`) + makeLineMidSection(borderCharSet, row, colWidths, padding) + borderCharSet(`v`);
    return (
      tableString +
      (rowIndex === 0 ? drawHorizontalBorder(borderCharSet, "t", colWidths, padding) + `\n` : ``) +
      ((
        (rowIndex === 1 && options?.headerBorder) ||
        (rowIndex > 0 && options?.allMiddleBorders)
      ) ? drawHorizontalBorder(borderCharSet, "m", colWidths, padding) + `\n` : ``) +
      line + `\n` +
      (rowIndex === height - 1 ? drawHorizontalBorder(borderCharSet, "b", colWidths, padding) + `\n` : ``)
    );
  };
}

/**
 * Draws the central part of a table line, that is, all except left and right borders.
 * @param {CharGetter} borderCharSet Border character getter
 * @param {Array<String>} row a row of string columns
 * @param {Array<Number>} colWidths number of columns in row
 * @param {Number} padding
 * @returns {String}
 */
function makeLineMidSection(borderCharSet, row, colWidths, padding) {
  return row
    .map(
      (v, i) =>
        ``.padEnd(padding, " ") +
        v.padEnd(colWidths[i], " ") +
        ``.padEnd(padding, " ")
    )
    .join(borderCharSet(`v`));
}

/**
 * Draws a horizontal border.
 * @param {CharGetter} borderCharSet Border character getter
 * @param {"t"|"m"|"b"} letter One letter code representing the border position: top = t, middle = m & bottom = b.
 * @param {Array<Number>} colWidths Array of column widths
 * @param {Number} padding
 * @returns {String}
 */
function drawHorizontalBorder(borderCharSet, letter, colWidths, padding) {
  return borderCharSet(/** @type {PieceName} */(letter + "l")) +
    colWidths
      .map((w) => "".padEnd(w + 2 * padding, borderCharSet(`h`)))
      .join(borderCharSet(/** @type {PieceName} */(letter + "c"))) +
    borderCharSet(/** @type {PieceName} */(letter + "r"));
}
